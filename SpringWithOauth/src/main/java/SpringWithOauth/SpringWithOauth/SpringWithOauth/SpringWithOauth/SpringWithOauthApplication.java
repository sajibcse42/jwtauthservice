package SpringWithOauth.SpringWithOauth.SpringWithOauth.SpringWithOauth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;


@SpringBootApplication
@EnableOAuth2Sso
public class SpringWithOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWithOauthApplication.class, args);
	}

}
