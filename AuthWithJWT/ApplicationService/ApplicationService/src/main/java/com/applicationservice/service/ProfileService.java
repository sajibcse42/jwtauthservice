package com.applicationservice.service;

import com.applicationservice.json.ProfileRequest;
import com.applicationservice.json.ProfileResponse;

public interface ProfileService {
	public ProfileResponse addProfile(ProfileRequest request);
	public ProfileResponse updateProfile(ProfileRequest profileId);
	public ProfileRequest getProfile(Long profileId);
	public void deleteProfile(Long profileId);
}
