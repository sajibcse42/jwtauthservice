package com.applicationservice.config;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.applicationservice.entity.User;
import com.applicationservice.repository.IUserRepository;

@Service
public class MyuserDetailsService implements UserDetailsService {

	@Autowired
	IUserRepository userRepository;
	@Override
	public UserDetails loadUserByUsername(String uid) throws UsernameNotFoundException {
		User user =  userRepository.findByuid(uid);
        if (user == null) {
            throw new UsernameNotFoundException(uid);
        }
        return new MyUserDetails(user);
	}

}
