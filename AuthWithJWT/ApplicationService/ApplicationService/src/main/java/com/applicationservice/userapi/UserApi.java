package com.applicationservice.userapi;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.applicationservice.json.ProfileRequest;

public interface UserApi {
	@PostMapping("/profile")
	public void addProfile(@RequestBody ProfileRequest request);
	
	@PutMapping("/profile")
	public void updateProfile(@RequestBody ProfileRequest request);
	
	@GetMapping("/profile")
	public ProfileRequest getProfile(@RequestParam Long profileId);
	
	@DeleteMapping("/profile")
	public void deleteProfile(@RequestParam Long profileId);

}
