package com.applicationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.applicationservice.json.ProfileRequest;
import com.applicationservice.service.ProfileService;

@RestController
public class HelloWorld {
	@Autowired ProfileService profileService;
	
	@RequestMapping("/Hello")
   	public String Hello() {
		return ("Hello World");
	}
	@RequestMapping("/addProfile")
	public void addProfile(@RequestBody ProfileRequest request) {
		profileService.addProfile(request);
	}

	@RequestMapping("/updateProfile")
	public void updateProfile(@RequestBody ProfileRequest request) {
	    profileService.updateProfile(request);
	}

	@GetMapping("/getProfile")
	public ProfileRequest getProfile(@RequestParam Long profileId) {
		
		return profileService.getProfile(profileId);
	}

	@RequestMapping("/deleteProfile")
	public void deleteProfile(@RequestParam Long profileId) {
		// TODO Auto-generated method stub
		profileService.deleteProfile(profileId);
	}

}
