package com.applicationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.applicationservice.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {
    User findByemail(String email);
    User findByuid(String uid);
}
