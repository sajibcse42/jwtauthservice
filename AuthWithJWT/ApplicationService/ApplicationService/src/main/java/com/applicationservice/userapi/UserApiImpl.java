package com.applicationservice.userapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.applicationservice.json.ProfileRequest;
import com.applicationservice.service.ProfileService;

@RestController
public class UserApiImpl implements UserApi {
	
	@Autowired ProfileService profileService;

	@Override
	public void addProfile(ProfileRequest request) {
		profileService.addProfile(request);
	}

	@Override
	public void updateProfile(ProfileRequest request) {
	    profileService.updateProfile(request);
	}

	@Override
	public ProfileRequest getProfile(Long profileId) {
		
		return profileService.getProfile(profileId);
	}

	@Override
	public void deleteProfile(Long profileId) {
		// TODO Auto-generated method stub
		profileService.deleteProfile(profileId);
	}

}
